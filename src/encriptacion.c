#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "funciones_cripto.h"

int cambio_valor(int limite_inferior, int limite_superior, int pos, int llave){
    if( llave > 0 ){
        for ( int i = 1; i <= llave; i++ ){
            if( pos < limite_superior ){
                pos = pos+1;
            }else{
                pos = limite_inferior;
            }
        }
    } else {
        for (int i = 1; i <= (llave*-1); i++ ){
            if(pos > limite_inferior){
                pos = pos-1;
            }else{
                pos = limite_superior;
            }
        }

    }
    return pos;
}

char *cifrado_ciclico(char *mensaje, int llave){
	int limite_superior = 90;
	int limite_inferior = 65;
	int limite_inferior_min = 97;
	int limite_superior_min = 122;
    for(int i=0; *(mensaje+i) != '\0'; i++){
        if( *(mensaje+i) >= limite_inferior && *(mensaje+i) <= limite_superior ){
            *(mensaje+i)=cambio_valor(limite_inferior,limite_superior,*(mensaje+i),llave);
        } else {
            if( *(mensaje+i) >= limite_inferior_min && *(mensaje+i) <= limite_superior_min ) {
                *(mensaje+i) = cambio_valor( limite_inferior_min,limite_superior_min,*(mensaje+i) ,llave);
            }
        }
    }
	return mensaje;
}

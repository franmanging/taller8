all:estatico dinamico

estatico: main.o libreria.a
	gcc -Wall obj/main.o lib/libreria.a -o bin/estatico

dinamico: main.o libcriptops.so
	gcc -Wall obj/main.o lib/libcriptops.so -o bin/dinamico

libreria.a: morse.o encriptacion.o 
	ar rcs lib/libreria.a obj/morse.o obj/encriptacion.o

libcriptops.so: morse_din.o encriptacion_din.o
	ld -o lib/libcriptops.so obj/morse_din.o obj/encriptacion_din.o -shared
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:lib/

main.o: src/main.c
	gcc -Wall -c -Iinclude/ src/main.c -o obj/main.o

encriptacion.o: src/encriptacion.c
	gcc -Wall -c -Iinclude/ src/encriptacion.c -o obj/encriptacion.o 

morse.o: src/morse.c
	gcc -Wall -c -Iinclude/ src/morse.c -o obj/morse.o

encriptacion_din.o: src/encriptacion.c
	gcc -Wall -fPIC -c -Iinclude/ src/encriptacion.c -o obj/encriptacion_din.o

morse_din.o: src/morse.c
	gcc -Wall -c -fPIC -Iinclude/ src/morse.c -o obj/morse_din.o

clean:
	rm obj/*  bin/*

.PHONY: all

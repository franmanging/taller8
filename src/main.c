#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "funciones_cripto.h"
void limpiar_buffer(void);
int main(int argc, char const *argv[]) {
	bool string_invalido = false;
  	bool numero_invalido = false;
	char mensaje[80] = {0};
  	int llave = 0;
  	int mensaje_tmp;
  	printf("Cifrado ciclico\n");
    do {
      memset(mensaje, 0, sizeof(mensaje));
      if (numero_invalido) {
        printf("\n>>>Numero no valido\n");
      }
      numero_invalido = false;
      if (string_invalido) {
        printf("\n>>>String no valido \n");
      }

      printf("Ingrese el mensaje a cifrar: ");
      fgets(mensaje, sizeof(mensaje), stdin);
      for (int i = 0; i < sizeof(mensaje); i++) {
        mensaje_tmp = (int)mensaje[i];
        if ( (mensaje_tmp < 65 || ( mensaje_tmp > 90 && mensaje_tmp < 97 ) || mensaje_tmp > 122 ) &&  mensaje_tmp!=10 && mensaje_tmp!= 0 && mensaje_tmp!= 32 && mensaje_tmp!= -1 && mensaje_tmp!= 47) {
          printf("%d\n",mensaje_tmp);
          string_invalido = true;
          break;
        } else {
          string_invalido = false;
        }
      }
      printf("Ingrese la llave numerica: ");
      scanf("%d", &llave);
      if ( llave == 0) {
        numero_invalido = true;
      }
      // limpiar buffer
      limpiar_buffer();
  	  // limpiar buffer
    } while(llave == 0 || (int)mensaje[0] == 10 || string_invalido == true);
    char * mensaje_cifrado = cifrado_ciclico(mensaje, llave);
    char * mensaje_morse = convertir_a_morse(mensaje);
    printf("Mensaje Cifrado: %s", mensaje_cifrado);
    printf("Mensaje en morse: %s\n", mensaje_morse);
}


void limpiar_buffer(void) {
  int c;
  do {
    c = getchar();
  } while (c != '\n' && c != EOF);
}
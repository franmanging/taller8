#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "funciones_cripto.h"

char * convertir_a_morse(char *msg)
{	
	char *respuesta = (char *)malloc(sizeof(char));
	// char *respuesta = malloc(sizeof());
	int l = strlen(msg);
	for(int i=0;i<l;i++)
	{
		int c = msg[i]; /* manejo con ascii */
		if(c>96)
		{
			c=c-32;
		}
		switch(c) /* evaluacion con cada letra ya convertidas a mayusculas */
		{
			case 'A': 
				strcat(respuesta," .-");
				break;
			case 'B':
				strcat(respuesta," -...");
				break;
			case 'C':
				strcat(respuesta," -.-.");
				break;
			case 'D':
				strcat(respuesta," -..");
				break;
			case 'E':
				strcat(respuesta," .");
				break;
			case 'F':
				strcat(respuesta," ..-.");
				break;
			case 'G':
				strcat(respuesta," --.");
				break;
			case 'H':
				strcat(respuesta," ....");
				break;
			case 'I':
				strcat(respuesta," ..");
				break;
			case 'J':
				strcat(respuesta," .---");
				break;		 
 			case 'K':
				strcat(respuesta," -.-");
				break;
			case 'L':
				strcat(respuesta," .-..");
				break;
			case 'M':
				strcat(respuesta," --");
				break;
			case 'N':
				strcat(respuesta," -.");
				break;
			case 'O':
				strcat(respuesta," ----");
				break;
			case 'P':
				strcat(respuesta," .--.");
				break;
			case 'Q':
				strcat(respuesta," --.-");
				break;
			case 'R':
				strcat(respuesta," .-.");
				break;
			case 'S':
				strcat(respuesta," ...");
				break;
			case 'T':
				strcat(respuesta," -");
				break;
			case 'U':
				strcat(respuesta," ..-");
				break;
			case 'V':
				strcat(respuesta," ...-");
				break; 
			case 'W':
				strcat(respuesta," .--");
				break;
			case 'X':
				strcat(respuesta," -..-");
				break;
			case 'Y':
				strcat(respuesta," -.--");
				break;
			case 'Z':
				strcat(respuesta," --..");
				break;
			case '1':
				strcat(respuesta," .----");
				break;
			case '2':
				strcat(respuesta," ..---");
				break;
			case '3':
				strcat(respuesta," ...--");
				break;
			case '4':
				strcat(respuesta," ....-");
				break;
			case '5':
				strcat(respuesta," .....");
				break;
			case '6':
				strcat(respuesta," -....");
				break;
			case '7':
				strcat(respuesta," --...");
				break;
			case '8':
				strcat(respuesta," ---..");
				break;
			case '9':
				strcat(respuesta," ----.");
				break;
			case '0':
				strcat(respuesta," -----");
				break;
			case ' ':
				strcat(respuesta," /");
				break;

			default:
				strcat(respuesta,"$");
		}
	}
	return respuesta;
}
